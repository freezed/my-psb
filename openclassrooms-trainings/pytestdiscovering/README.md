# Discovering [pytest](https://docs.pytest.org/en/latest/getting-started.html) with [Sam & Max](http://sametmax.com/tag/pytest/) & [Open Classrooms](https://openclassrooms.com/fr/courses/4425126-testez-votre-projet-avec-python "Testez votre projet avec Python")

This is a (partial) fork of my _study work_ [ocp5](https://github.com/freezed/ocp5/blob/master/README.md) providing code for discovering [`pytest`](https://docs.pytest.org/en/latest/contents.html).

Work is done with :

 - `Python 3.6.4`
 - `PyMySQL 0.9.2`
 - `requests 2.11.1`
 - `pytest 3.7.1`
